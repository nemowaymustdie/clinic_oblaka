package kg.hazakura.clinic_oblaka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClinicOblakaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClinicOblakaApplication.class, args);
    }

}
